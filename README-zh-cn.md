#  openeuler-jenkins

## 简介

此仓库用来存放openEuler社区的Jenkins脚本。

## 许可证

详情请参考[LICENSE](https://gitee.com/openeuler/openeuler-jenkins/blob/ac397ce3e078937c700df6fb8de0e1b065ee4218/LICENSE)文件。